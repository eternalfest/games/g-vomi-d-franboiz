package g_vomi_d_pakages;

import hf.entity.Bad;
import hf.mode.GameMode;
import hf.Hf;
import patchman.IPatch;
import patchman.Ref;

@:build(patchman.Build.di())
class NinjuOverkill {
    @:diExport
    public var set_all_bads_to_hearts(default, null): IPatch;

    public function new(): Void {
        this.set_all_bads_to_hearts = Ref.auto(GameMode.onBadsReady).after(function(hf: Hf, self: GameMode): Void {
            if (!self.fl_ninja)
                return;
            for (bad_entity in self.getList(hf.Data.BAD_CLEAR)) {
                var bad: hf.entity.Bad = cast bad_entity;
                if (!bad.fl_ninFoe)
                    bad.fl_ninFriend = true;
            }
        });
    }
}
